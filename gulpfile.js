const gulp = require("gulp");
const del = require("del");
gulp.task("clean", () => del(["build/**/*"]));
gulp.task("json", () => gulp.src("src/**/*.+(json)").pipe(gulp.dest("build/")));
